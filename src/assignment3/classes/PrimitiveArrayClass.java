package assignment3.classes;

import java.util.Scanner;

public class PrimitiveArrayClass {
	public int[] array;
	
	public static PrimitiveArrayClass createFromCmdLine() {
		PrimitiveArrayClass obj = new PrimitiveArrayClass();
		obj.array = new int[3];
		
		Scanner scan = new Scanner(System.in);
		for(int i = 0; i < obj.array.length; i++) {
			System.out.println("Enter an int (" + i+1 + "/" + obj.array.length + "): ");
			obj.array[i] = scan.nextInt();
		}
		
		return obj;
	}
}
