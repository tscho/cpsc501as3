package assignment3.classes;

import java.util.InputMismatchException;
import java.util.Scanner;

public class ReferencingClass {
	public Object ref1;
	public Object ref2;
	public Object ref3;

	private ReferencingClass() { }
	public ReferencingClass(Object ref1, Object ref2, Object ref3) {
		this.ref1 = ref1;
		this.ref2 = ref2;
		this.ref3 = ref3;
	}

	public static ReferencingClass createFromCmdLine(Object rootObj) {
  	ReferencingClass ref = new ReferencingClass(null, null, null);
  	
  	System.out.println("Creating a referencing class");
  	
  	ref.ref1 = PrimitiveClass.createFromCmdLine();
  	ref.ref2 = PrimitiveClass.createFromCmdLine();
  	
  	Scanner scan = new Scanner(System.in);
  	System.out.print("Enter 'n' for null, 'c' for cyclic, 'r' for referencing, 'p' for primitive: ");
  	char choice = scan.next().charAt(0);
  	switch(choice) {
  	case 'n':
  		ref.ref3 = null;
  		break;
  	case 'c':
  		ref.ref3 = rootObj;
  		break;
  	case 'r':
  		ref.ref3 = ReferencingClass.createFromCmdLine(rootObj == null ? ref : rootObj);
  		break;
  	case 'p':
  		ref.ref3 = PrimitiveClass.createFromCmdLine();
  		break;
  	default:
  		throw new InputMismatchException();
  	}
  	
  	return ref;
	}
}
