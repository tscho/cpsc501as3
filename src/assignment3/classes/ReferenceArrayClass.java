package assignment3.classes;

public class ReferenceArrayClass {
	public PrimitiveClass[] array;
	
	public static ReferenceArrayClass createFromCmdLine() {
		ReferenceArrayClass rarr = new ReferenceArrayClass();
		rarr.array = new PrimitiveClass[2];
		
		for(int i = 0; i < rarr.array.length; i++) {
			rarr.array[i] = PrimitiveClass.createFromCmdLine();
		}
		
		return rarr;
	}
}
