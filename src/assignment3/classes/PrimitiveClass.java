package assignment3.classes;

import java.util.Scanner;

public class PrimitiveClass {
	public int anInt;
	public double aDouble;
	private char aChar;
	public boolean aBool;
	
	private PrimitiveClass() { }
	public PrimitiveClass(int anInt, double aDouble, char aChar, boolean aBool) {
		this.anInt = anInt;
		this.aDouble = aDouble;
		this.aChar = aChar;
		this.aBool = aBool;
	}
	
	public char getAChar() {
		return aChar;
	}
	
	public static PrimitiveClass createFromCmdLine() {
  	System.out.println("Creating a primitive class");
  	
		Scanner input = new Scanner(System.in);
		System.out.print("Enter an int: ");
		int anInt = input.nextInt();
		System.out.print("Enter a double: ");
		double aDouble = input.nextDouble();
		System.out.print("Enter a char: ");
		char aChar = input.next().charAt(0);
		System.out.print("Enter a boolean: ");
		boolean aBool = input.nextBoolean();
		
		return new PrimitiveClass(anInt, aDouble, aChar, aBool);
	}
}
