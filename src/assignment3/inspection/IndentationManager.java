package assignment3.inspection;

public class IndentationManager {
	private String indentation;
	private String indentUnit;
	
	public IndentationManager() {
		indentation = "";
		indentUnit = "\t";
	}
	
	public IndentationManager(int startIndent) {
		this();
		incIndent(startIndent);
	}
	
	public IndentationManager(String unit) {
		this();
		indentUnit = unit;
	}
	
	public IndentationManager(String unit, int startIndent) {
		this(startIndent);
		indentUnit = unit;
	}
	
	public void resetIndent() {
		indentation = "";
	}
	
	public void incIndent() { incIndent(1); }
	public void incIndent(int num) {
		while(num > 0) {
			indentation += indentUnit;
			num--;
		}
	}
	
	public void decIndent() { decIndent(1); }
	public void decIndent(int num) {
		while(num > 0) {
			if(indentation.length() >= indentUnit.length())
				indentation = indentation.substring(indentUnit.length());
			num--;
		}
	}
	
	public String getIndent() {
		return indentation;
	}
	
	public String indent(String src) {
		return indentation + src;
	}
}
