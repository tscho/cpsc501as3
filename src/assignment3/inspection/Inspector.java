/*=========================================
 * Copyright 2010 Thomas Schoendorfer
 * 
 * Produced for CPSC501 at the University Of Calgary
 ==========================================*/
package assignment3.inspection;

import java.lang.reflect.*;
import java.util.IdentityHashMap;
import java.io.PrintStream;

public class Inspector {
	public static PrintStream output = System.out;
	public static IndentationManager indentor;
	
	public static void inspect(Object obj, boolean recursive) {
		indentor = new IndentationManager("  ");
		
		Class classObj = obj.getClass();
		inspect(obj, classObj, recursive, new IdentityHashMap<Object, Integer>());
	}
	
	private static void inspect(Object obj, Class classObj, boolean recursive, IdentityHashMap<Object, Integer> inspected) {
		output.println(indentor.indent(classObj.getCanonicalName()));
		
		if(inspected.get(obj) == null) {
			inspected.put(obj, inspected.size());
		}
		
		output.println(indentor.indent(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>BEGIN " + classObj.getSimpleName() + 
				" id=" + inspected.get(obj) + ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"));
		
		printSuperclass(obj, classObj.getSuperclass(), recursive, inspected);
		printFields(obj, classObj.getDeclaredFields(), recursive, inspected);
		if(classObj.isArray()) { printArrayContents(obj, classObj, recursive, inspected); }

		output.println(indentor.indent("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<END " + classObj.getSimpleName() + "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<"));
	}

	private static void printSuperclass(Object obj, Class superClassObj, boolean recursive, IdentityHashMap<Object, Integer> inspected) {
		output.println(String.format("%sSuperclass: %s", indentor.getIndent(), superClassObj != null ? superClassObj.getCanonicalName() : "None"));
		if(superClassObj == null) { return; }
		
		indentor.incIndent();
		inspect(obj, superClassObj, recursive, inspected);
		indentor.decIndent();
	}

	private static void printFields(Object obj, Field[] fields, boolean recursive, IdentityHashMap<Object, Integer> inspected) {
		output.println(indentor.indent("Fields:" + (fields.length == 0 ? " None" : "")));
		indentor.incIndent();
		for(Field f : fields) {
			output.print(String.format("%s%s %s %s ", indentor.getIndent(), Modifier.toString(f.getModifiers()), f.getType().getSimpleName(), f.getName()));
			f.setAccessible(true);
			try {
				Object val = f.get(obj);
				
				if(val == null) { 
					output.println("= null");
					continue; 
				}
				
				Class valClass = val.getClass();
				
				if(recursive && !f.getType().isPrimitive()) {
					output.println();
					indentor.incIndent();
					if(!inspected.containsKey(val)) {
						inspect(val, valClass, recursive, inspected);
					}
					else {
						output.println(indentor.indent("Already inspected, id=" + inspected.get(val)));
					}
					indentor.decIndent();
				}
				else
					if(valClass.isArray()) {
						output.println();
						printArrayContents(val, valClass, false, inspected);
					}
					else
						output.println(String.format("= %s", val));
			} catch (IllegalAccessException e) {
				output.println("Illegal Access!?! This should never happen: " + e.getMessage());
			}
		}
		indentor.decIndent();
	}
	
	private static void printArrayContents(Object obj, Class classObj, boolean recursive, IdentityHashMap<Object, Integer> inspected) {
		output.println(indentor.indent("Array Elements:"));
		int length = 0;
		try {
			length = Array.getLength(obj);
		} catch(Exception e) {
			output.println("Couldn't get array length: " + e.getMessage());
		}
		indentor.incIndent();
		for(int i = 0; i < length; i++) {
			Object currObj = Array.get(obj, i);
			if(recursive && !classObj.getComponentType().isPrimitive()) {
				if(currObj == null) {
					output.println(String.format("%sElement %d: null", indentor.getIndent(), i));
					continue;
				}
				output.println(String.format("%sElement %d:", indentor.getIndent(), i));
				indentor.incIndent();
				if(!inspected.containsKey(currObj)) {
					inspect(currObj, currObj.getClass(), recursive, inspected);
				}
				else {
					output.println(indentor.indent("Already inspected, id=" + inspected.get(currObj)));
				}
				indentor.decIndent();
			}
			else {
				output.println(String.format("%sElement %d: %s", indentor.getIndent(), i, currObj));
			}
		}
		indentor.decIndent();
	}
}
