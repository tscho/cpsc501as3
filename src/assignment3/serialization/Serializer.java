package assignment3.serialization;

import org.jdom.*;

import java.util.*;
import java.lang.reflect.*;

public class Serializer {
	public static Document serialize(Object obj) {
		Document doc = new Document(new Element("serialized"));
		
		IdentityHashMap<Object, Integer> objects = new IdentityHashMap<Object, Integer>();
		
		try {
			findObjects(obj, objects);
		} catch (IllegalAccessException e) {
			System.out.println("Error while finding objects: " + e.getMessage());
		}
		
		for(Object currObj : objects.keySet()) {
			try {
				doc.getRootElement().addContent(serializeObject(currObj, objects));
			} catch (IllegalAccessException e) {
				System.out.println("Error while serializing objects: " + e.getMessage());
			}
		}
		
		return doc;
	}
	
	private static void findObjects(Object obj, IdentityHashMap<Object, Integer> objects) 
	throws IllegalAccessException {
		if(obj == null) {
			return;
		}
		
		if(objects.get(obj) == null) {
			objects.put(obj, objects.size());
		}
		else {
			return;
		}
		
		if(obj.getClass().isArray() && !obj.getClass().getComponentType().isPrimitive()) {
			for(int i = 0; i < Array.getLength(obj); i++) {
				findObjects(Array.get(obj, i), objects);
			}
		}
		
		for(Field f : getFields(obj.getClass())) {
			if(!Modifier.isPublic(f.getModifiers())) {
				f.setAccessible(true);
			}
				
			Object val = f.get(obj);
			if(val == null)
				continue;
			
			Class fieldClass = f.getType();
			
			if(!fieldClass.isPrimitive()) {
				findObjects(val, objects);
			}
		}
	}

	private static Element serializeObject(Object obj, IdentityHashMap<Object, Integer> objects)
	throws IllegalAccessException {
		Class classObj = obj.getClass();
		Element objElement = new Element("object");
		objElement.setAttribute("class", classObj.getName());
		objElement.setAttribute("id", Integer.toString(objects.get(obj)));
		
		if(classObj.isArray()) {
			objElement.setAttribute("length", Integer.toString(Array.getLength(obj)));
			if(classObj.getComponentType().isPrimitive()) {
				for(int i = 0; i < Array.getLength(obj); i++) {
					Element valElement = new Element("value");
					valElement.addContent(Array.get(obj, i).toString());
					objElement.addContent(valElement);
				}
			}
			else {
				for(int i = 0; i < Array.getLength(obj); i++) {
					Element refElement = new Element("reference");
					Object fVal = Array.get(obj, i);
					refElement.addContent(fVal == null ? "-1" : objects.get(fVal).toString());
					objElement.addContent(refElement);
				}
			}
		}
		
		List<Field> fields = getFields(classObj);
		for(Field f : fields) {
			if(!Modifier.isPublic(f.getModifiers())) {
				f.setAccessible(true);
			}
			
			Element fElement = new Element("field");
			fElement.setAttribute("name", f.getName());
			fElement.setAttribute("declaringclass", f.getDeclaringClass().getName());
			
			if(f.getType().isPrimitive()) {
				Element valElement = new Element("value");
				valElement.addContent(f.get(obj).toString());
				fElement.addContent(valElement);
			}
			else {
				Element refElement = new Element("reference");
				
				Object fVal = f.get(obj); 

				refElement.addContent(fVal == null ? "-1" : objects.get(fVal).toString());
				fElement.addContent(refElement);
			}
			
			objElement.addContent(fElement);
		}
		
		return objElement;
	}

	private static List<Field> getFields(Class classObj) {
		Stack<Field> fields = new Stack<Field>();
		
		while(classObj != null) {
			for(Field f : classObj.getDeclaredFields()) {
				if(!Modifier.isStatic(f.getModifiers())) {
					fields.push(f);
				}
			}
			classObj = classObj.getSuperclass();
		}
		
		return fields;
	}
}
