package assignment3.serialization;

import org.jdom.*;

import java.util.*;
import java.lang.reflect.*;

public class Deserializer {
	public static Object deserialize(Document doc) throws Throwable {
		HashMap<Integer, Object> objects = new HashMap<Integer, Object>();
		
		List<Element> objElements = doc.getRootElement().getChildren();
		
		loadObjects(objElements, objects);
		
		assignFields(objElements, objects);

		return objects.get(0);
	}

	private static void loadObjects(List<Element> objectElements, HashMap<Integer, Object> objects)
	throws Exception {
		for(Element el : objectElements) {
			Class currClassObj = Class.forName(el.getAttributeValue("class"));
			Object currObj = null;
			
			if(!currClassObj.isArray()) {
				Object primClassObject = createPrimitiveObject(currClassObj);
				if(primClassObject == null) {
					Constructor c = currClassObj.getDeclaredConstructor(null);
					
					if(!Modifier.isPublic(c.getModifiers())){
						c.setAccessible(true);
					}
					
					currObj = c.newInstance(null);
				} else {
					currObj = primClassObject;
				}
			} else {
				currObj = Array.newInstance(currClassObj.getComponentType(), Integer.parseInt(el.getAttributeValue("length")));
			}
			
			objects.put(Integer.parseInt(el.getAttributeValue("id")), currObj);
		}
	}
	
	private static void assignFields(List<Element> objElements, HashMap<Integer, Object> objects) 
	throws Exception {
		for(Element el : objElements) {
			Object currObj = objects.get(Integer.parseInt(el.getAttributeValue("id")));
			Class currClassObj = currObj.getClass();
			List<Element> fieldElements = el.getChildren();
			
			if(!currClassObj.isArray()) {
				for(Element fEl : fieldElements) {
					Class declaringClassObj = Class.forName(fEl.getAttributeValue("declaringclass"));
					Field f = declaringClassObj.getDeclaredField(fEl.getAttributeValue("name"));
					
					if(!Modifier.isPublic(f.getModifiers())) {
						f.setAccessible(true);
					}
					
					f.set(currObj, deserializeField(objects, f.getType(), (Element)fEl.getChildren().get(0)));
				}
			}
			else {
				Class compType = currClassObj.getComponentType();
				List<Element> arrayElements = el.getChildren(compType.isPrimitive() ? "value" : "reference");
				for(int i = 0; i < Integer.parseInt(el.getAttributeValue("length")); i++) {
					Array.set(currObj, i, deserializeField(objects, compType, arrayElements.get(i)));
				}
			}
		}
	}

	private static Object deserializeField(HashMap<Integer, Object> objects, Class fieldType, Element vEl) {
		if(fieldType.isPrimitive()) {
			String value = vEl.getText();
			if(fieldType.equals(byte.class)) {
				return Byte.valueOf(value);
			} else if (fieldType.equals(boolean.class)) { 
				return value.equals("true");
			} else if (fieldType.equals(char.class)) {
				return new Character(value.charAt(0));
			} else if (fieldType.equals(double.class)) {
				return Double.valueOf(value);
			} else if (fieldType.equals(float.class)) {
				return Float.valueOf(value);
			} else if (fieldType.equals(int.class)) {
				return Integer.valueOf(value);
			} else if (fieldType.equals(long.class)) {
				return Long.valueOf(value);
			} else if (fieldType.equals(short.class)) {
				return Short.valueOf(value);
			}
			
		} else {
			int refValue = Integer.parseInt(vEl.getText());
			if(refValue == -1) {
				return null;
			}
			
			return objects.get(refValue);
		}
		
		return null;
	}
	
	private static Object createPrimitiveObject(Class classObj) {
		if(classObj.equals(byte.class)) {
			return new Byte((byte)0);
		} else if (classObj.equals(Boolean.class)) { 
			return new Boolean(false);
		} else if (classObj.equals(Character.class)) {
			return new Character('\n');
		} else if (classObj.equals(Double.class)) {
			return new Double(0);
		} else if (classObj.equals(Float.class)) {
			return new Float(0);
		} else if (classObj.equals(Integer.class)) {
			return new Integer(0);
		} else if (classObj.equals(Long.class)) {
			return new Long(0);
		} else if (classObj.equals(Short.class)) {
			return new Short((short)0);
		}
		
		return null;
	}
}
