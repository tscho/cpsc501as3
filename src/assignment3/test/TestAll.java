package assignment3.test;

import junit.framework.*;

public class TestAll extends TestCase {
	 static public Test suite() {
		 TestSuite suite = new TestSuite();
		 
		 suite.addTestSuite(TestSerializer.class);
		 suite.addTestSuite(TestDeserializer.class);
		 suite.addTestSuite(TestMenuCommand.class);
		 
		 return suite;
	 }
}
