package assignment3.test;

import junit.framework.*;
import assignment3.menu.*;


public class TestMenuCommand extends TestCase
{
  boolean verifier;

  public void testInvoke() {
    try {
      MenuCommand mc = new MenuCommand("Test",  
        getClass().getDeclaredMethod("method", new Class[] {String.class}), this);
      mc.invoke(new Object[] {"testing"});
    } catch (Exception e) {
      fail(e.getMessage());
    }
  }

  public void method(String a) {
    a.isEmpty();
  }
  
  public void testExec() {
	  verifier = false;
	  try {
		  java.lang.reflect.Method m = getClass().getDeclaredMethod("method", new Class[] {String.class});
		  MenuCommand mc = new MenuCommand("Test", m, this);
		  mc.nextCommand = new MenuCommand("Test2", m, this);
		  mc.nextCommand.nextCommand = new MenuCommand("Tester", getClass().getDeclaredMethod("verify", null), this);
		  mc.execCommand(2);
		  assertTrue(verifier);
	  } catch (Exception e) {
		  fail(e.getMessage());
	  }
  }
  
  public void verify() {
	  verifier = true;
  }
  
	public static Test suite() {
		TestSuite suite = new TestSuite();
		
		suite.addTest(new TestMenuCommand());
		
		return suite;
	}
}
