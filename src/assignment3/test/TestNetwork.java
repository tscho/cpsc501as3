package assignment3.test;

import assignment3.*;
import assignment3.classes.*;
import assignment3.net.*;
import assignment3.serialization.Serializer;
import junit.framework.*;

import java.io.*;

import org.jdom.Document;

public class TestNetwork extends TestCase {
	private boolean accepted;
	
	public void setUp() {
		accepted = false;
	}
	
	public void testSend() {
		try {
			Client.send(Serializer.serialize(new PrimitiveClass(1, 1, '1', false)), "localhost", 44444);
		} catch (IOException e) {
			fail();
		}
	}
	
	public void testReferencingCyclic() {
		PrimitiveClass prim1 = new PrimitiveClass(1,1,'1', false);
		ReferencingClass testCls = new ReferencingClass(
				prim1,
				null, null);
		testCls.ref3 = new ReferencingClass(new PrimitiveClass(2, 2, '2', true), testCls, prim1);
		
		Document doc = Serializer.serialize(testCls);
		
		try {
			Client.send(doc, "localhost", 44444);
		} catch (IOException e) {
			fail();
		}
	}
	
	public static Test suite() {
		TestSuite suite = new TestSuite();
		
		suite.addTest(new TestDeserializer());
		
		return suite;
	}
}
