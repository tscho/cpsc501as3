package assignment3.test;

import org.jdom.Document;

import assignment3.classes.*;
import assignment3.serialization.*;
import junit.framework.*;

public class TestDeserializer extends TestCase {
	
	public void testNull() throws Throwable {
		Document doc = Serializer.serialize(null);
		Object result = Deserializer.deserialize(doc);
		assertEquals(null, result);
	}
	
	public void testPrimitive() throws Throwable {
		PrimitiveClass testCls = new PrimitiveClass(1, 2.3, '4', false);
		Document doc = Serializer.serialize(testCls);

		PrimitiveClass result = (PrimitiveClass)Deserializer.deserialize(doc);
		assertEquals(1, result.anInt);
		assertEquals(2.3, result.aDouble);
		assertEquals('4', result.getAChar());
		assertEquals(false, result.aBool);
	}
	
	public void testReferencingSimple() throws Throwable {
		ReferencingClass testCls = new ReferencingClass(
				new PrimitiveClass(1, 1, '1', true),
				null,
				new PrimitiveClass(3, 3, '3', true));
		
		Document doc = Serializer.serialize(testCls);
		
		ReferencingClass result = (ReferencingClass)Deserializer.deserialize(doc);
		assertEquals(null, result.ref2);
		
		PrimitiveClass prim1 = (PrimitiveClass)result.ref1;
		assertEquals(1, prim1.anInt);
		assertEquals(1.0, prim1.aDouble);
		assertEquals('1', prim1.getAChar());
		assertEquals(true, prim1.aBool);
		
		PrimitiveClass prim2 = (PrimitiveClass)result.ref3;
		assertEquals(3, prim2.anInt);
		assertEquals(3.0, prim2.aDouble);
		assertEquals('3', prim2.getAChar());
		assertEquals(true, prim2.aBool);
		
	}
	
	public void testReferencingCyclic() throws Throwable {
		PrimitiveClass primitive1 = new PrimitiveClass(1,1,'1', false);
		ReferencingClass testCls = new ReferencingClass(
				primitive1,
				null, null);
		testCls.ref3 = new ReferencingClass(new PrimitiveClass(2, 2, '2', true), testCls, primitive1);
		
		Document doc = Serializer.serialize(testCls);
		ReferencingClass result = (ReferencingClass)Deserializer.deserialize(doc);
		
		assertEquals(primitive1.anInt, ((PrimitiveClass)result.ref1).anInt);
		assertEquals(primitive1.aDouble, ((PrimitiveClass)result.ref1).aDouble);
		assertEquals(primitive1.getAChar(), ((PrimitiveClass)result.ref1).getAChar());
		assertEquals(primitive1.aBool, ((PrimitiveClass)result.ref1).aBool);
		
		PrimitiveClass prim2 = (PrimitiveClass)((ReferencingClass)result.ref3).ref1;
		assertEquals(2, prim2.anInt);
		assertEquals(2.0, prim2.aDouble);
		assertEquals('2', prim2.getAChar());
		assertEquals(true, prim2.aBool);
		
		assertEquals(result, ((ReferencingClass)(result.ref3)).ref2);
	}
	
	public void testPrimitiveArray() throws Throwable {
		int[] arr = new int[] {1,2,3,4};
		
		Document doc = Serializer.serialize(arr);
		int[] result = (int[])Deserializer.deserialize(doc);
		
		for(int i = 0; i < arr.length; i++) {
			assertEquals(arr[i], result[i]);
		}
	}
	
	public void testPrimitiveArrayClass() throws Throwable{
		PrimitiveArrayClass testCls = new PrimitiveArrayClass();
		testCls.array = new int[] {1, 2, 3, 4};
		
		PrimitiveArrayClass result = (PrimitiveArrayClass)Deserializer.deserialize(Serializer.serialize(testCls));

		for(int i = 0; i < result.array.length; i++) {
			assertEquals(testCls.array[i], result.array[i]);
		}
	}
	
	public void testReferencingArray() throws Throwable {
		PrimitiveClass[] arr = new PrimitiveClass[] {
				new PrimitiveClass(1, 1, '1', false),
				new PrimitiveClass(2, 2, '2', true),
				null,
				new PrimitiveClass(3, 3, '3', false) };
		
		Document doc = Serializer.serialize(arr);
		PrimitiveClass[] result = (PrimitiveClass[])Deserializer.deserialize(doc);
		
		for(int i = 0; i < arr.length; i++) {
			if(arr[i] == null) {
				continue;
			}
			
			assertEquals(arr[i].aBool, result[i].aBool);
			assertEquals(arr[i].aDouble, result[i].aDouble);
			assertEquals(arr[i].getAChar(), result[i].getAChar());
			assertEquals(arr[i].anInt, result[i].anInt);
		}
	}
	
	
	public void testCollection() throws Throwable {
		java.util.ArrayList<Object> testCls = new java.util.ArrayList<Object>();
		testCls.add(4);
		testCls.add(new PrimitiveClass(1, 1, '1', false));
		testCls.add(new PrimitiveClass(2, 2, '2', true));
		testCls.add(new ReferencingClass(null, new PrimitiveClass(3,3,'3', false), testCls));
		
		Document doc = Serializer.serialize(testCls);
		java.util.ArrayList<Object> result = (java.util.ArrayList<Object>)Deserializer.deserialize(doc);
		
		assertEquals(4, result.get(0));
		
		PrimitiveClass prim1 = (PrimitiveClass)result.get(1);
		assertEquals(1, prim1.anInt);
		assertEquals(1.0, prim1.aDouble);
		assertEquals('1', prim1.getAChar());
		assertEquals(false, prim1.aBool);
		
		PrimitiveClass prim2 = (PrimitiveClass)result.get(2);
		assertEquals(2, prim2.anInt);
		assertEquals(2.0, prim2.aDouble);
		assertEquals('2', prim2.getAChar());
		assertEquals(true, prim2.aBool);
		
		assertEquals(result, ((ReferencingClass)result.get(3)).ref3);
		
		PrimitiveClass prim3 = (PrimitiveClass)(((ReferencingClass)result.get(3)).ref2);
		assertEquals(3, prim3.anInt);
		assertEquals(3.0, prim3.aDouble);
		assertEquals('3', prim3.getAChar());
		assertEquals(false, prim3.aBool);
	}
	
	public static Test suite() {
		TestSuite suite = new TestSuite();
		
		suite.addTest(new TestDeserializer());
		
		return suite;
	}
}
