package assignment3.test;

import junit.framework.*;
import assignment3.*;
import assignment3.classes.*;
import assignment3.serialization.Serializer;
import org.jdom.*;
import org.jdom.output.*;

public class TestSerializer extends TestCase {
	public XMLOutputter output;
	
	public void setUp() {
		output = new XMLOutputter(Format.getPrettyFormat());
	}
	
	public void testNull() {
		Document doc = Serializer.serialize(null);
		assertEquals(0, doc.getRootElement().getChildren().size());
		System.out.print(output.outputString(doc));
	}
	
	public void testPrimitive() {
		PrimitiveClass testCls = new PrimitiveClass(1, 2.3, '4', false);
		Document doc = Serializer.serialize(testCls);
		assertEquals(1, doc.getRootElement().getChildren().size());
		System.out.print(output.outputString(doc));
	}
	
	public void testReferencingSimple() {
		ReferencingClass testCls = new ReferencingClass(
				new PrimitiveClass(1, 1, '1', true),
				null,
				new PrimitiveClass(3, 3, '3', true));
		
		Document doc = Serializer.serialize(testCls);
		assertEquals(3, doc.getRootElement().getChildren().size());
		System.out.print(output.outputString(doc));
	}
	
	public void testReferencingCyclic() {
		PrimitiveClass prim1 = new PrimitiveClass(1,1,'1', false);
		ReferencingClass testCls = new ReferencingClass(
				prim1,
				null, null);
		testCls.ref3 = new ReferencingClass(new PrimitiveClass(2, 2, '2', true), testCls, prim1);
		
		Document doc = Serializer.serialize(testCls);
		assertEquals(4, doc.getRootElement().getChildren().size());
		System.out.print(output.outputString(doc));
	}
	
	public void testPrimitiveArray() {
		int[] arr = new int[] {1,2,3,4};
		
		Document doc = Serializer.serialize(arr);
		assertEquals(1, doc.getRootElement().getChildren().size());
		System.out.print(output.outputString(doc));
	}
	
	public void testPrimitiveArrayClass() {
		PrimitiveArrayClass testCls = new PrimitiveArrayClass();
		testCls.array = new int[] {1, 2, 3, 4};
		
		Document doc = Serializer.serialize(testCls);
		assertEquals(2, doc.getRootElement().getChildren().size());
		System.out.print(output.outputString(doc));
	}
	
	public void testReferencingArray() {
		PrimitiveClass[] arr = new PrimitiveClass[] {
				new PrimitiveClass(1, 1, '1', false),
				new PrimitiveClass(2, 2, '2', true),
				null,
				new PrimitiveClass(3, 3, '3', false) };
		
		Document doc = Serializer.serialize(arr);
		assertEquals(4, doc.getRootElement().getChildren().size());
		System.out.print(output.outputString(doc));
	}
	
	public void testCollection() {
		java.util.ArrayList<Object> testCls = new java.util.ArrayList<Object>();
		testCls.add(4);
		testCls.add(new PrimitiveClass(1, 1, '1', false));
		testCls.add(new PrimitiveClass(2, 2, '2', true));
		testCls.add(new ReferencingClass(null, new PrimitiveClass(3,3,'3', false), testCls));
		
		Document doc = Serializer.serialize(testCls);
		System.out.print(output.outputString(doc));
		assertEquals(7, doc.getRootElement().getChildren().size());
	}
	
	public static Test suite() {
		TestSuite suite = new TestSuite();
		
		suite.addTest(new TestSerializer());
		
		return suite;
	}
}
