package assignment3;

import java.io.IOException;

import assignment3.net.*;
import assignment3.serialization.*;
import assignment3.inspection.*;

import org.jdom.*;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;

public class ObjectDisplayer implements DocumentAcceptor {
	private Server server;
	
	public ObjectDisplayer(int port) throws IOException {
		server = new Server(port);
		server.acceptor = this;
	}
	
	public void start() throws Throwable {
		server.listen();
	}
	
	public void accept(Document doc) throws Throwable {
		Object result = Deserializer.deserialize(doc);
		System.out.println("\n\n\n");
		
		XMLOutputter output = new XMLOutputter(Format.getPrettyFormat());
		output.outputString(doc);
		System.out.println("\n");
		
		Inspector.inspect(result, true);
	}
	
	public static void main(String[] args) throws Throwable {
		if(args.length < 1) {
			System.out.println("No port specified");
			return;
		}
		
		ObjectDisplayer od = new ObjectDisplayer(Integer.parseInt(args[0]));
		od.start();
	}
}
