package assignment3.menu;

import java.lang.reflect.*;
import java.util.Scanner;

public class MenuCommand
{
  private String prompt;
  private Method method;
  private Object instance;
  public MenuCommand nextCommand;

  public MenuCommand(String prompt, Method method, Object instance) {
    this.prompt = prompt;
    this.method = method;
    this.instance = instance;
  }

  public String getPrompt() {
    return prompt;
  }

  public void invoke(Object[] args) throws IllegalAccessException, InvocationTargetException {
      method.invoke(instance, args);
  }
  
  public void printMenu() { printMenu(1); }
  public void printMenu(int itemNumber) {
	  System.out.println(String.format("%d. %s", itemNumber, prompt));
	  if(nextCommand != null) { nextCommand.printMenu(++itemNumber); }
  }
  
  public void execCommand(int commandNum) throws IllegalAccessException, InvocationTargetException {
	  if(commandNum == 0) {
		  invoke(null);
	  }
	  else if(nextCommand != null) { nextCommand.execCommand(--commandNum); }
  }
  
  public static boolean runMenu(MenuCommand rootCmd) throws InvocationTargetException, IllegalAccessException {
  	Scanner input = new Scanner(System.in);
  	rootCmd.printMenu();
  	System.out.print("Enter command number or 'q' to quit: ");
  	String line = input.nextLine().toLowerCase().trim();
  	if(line.equals("q")) { return false; }
  	rootCmd.execCommand(Integer.parseInt(line)-1);
  	return true;
  }
}
