package assignment3;

import java.io.IOException;
import java.util.*;

import assignment3.classes.*;
import assignment3.menu.*;
import assignment3.net.Client;
import assignment3.serialization.*;

public class ObjectCreator {
  private HashMap<String, Object> objects;
  private MenuCommand rootCmd;
  private String host;
  private int port;
  private Object rootObject;
  
  public ObjectCreator(String host, int port) throws NoSuchMethodException{
  	objects = new HashMap<String, Object>();
  	this.host = host;
  	this.port = port;
  	
  	Class cls = getClass();
  	rootCmd = new MenuCommand("Create Class with Primitive Fields", cls.getDeclaredMethod("createPrimitive", null), this);
  	
  	LinkedList<MenuCommand> commands = new LinkedList<MenuCommand>();
  	commands.add(new MenuCommand("Create Class with Reference Fields", cls.getDeclaredMethod("createReference", null), this));
  	commands.add(new MenuCommand("Create Class containing primitive array", cls.getDeclaredMethod("createPArray", null), this));
  	commands.add(new MenuCommand("Create Class containing reference array", cls.getDeclaredMethod("createRArray", null), this));
  	commands.add(new MenuCommand("Create Collection Class", cls.getDeclaredMethod("createCollection", null), this));
  	
  	MenuCommand currCmd = rootCmd;
  	for(MenuCommand cmd : commands) {
  		currCmd.nextCommand = cmd;
  		currCmd = cmd;
  	}
  }

  public void createPrimitive() throws IOException {
  	try {
  		rootObject = PrimitiveClass.createFromCmdLine();
    	sendObjects();
    	rootObject = null;
  	} catch (InputMismatchException e) {
  		System.out.println("Invalid input");
  		return;
  	}
  }
  
  public void createReference() throws IOException {
  	try {
  		rootObject = ReferencingClass.createFromCmdLine(null);
    	sendObjects();
    	rootObject = null;
  	} catch (InputMismatchException e) {
  		System.out.println("Invalid input");
  		return;
  	}
  }
  
  public void createPArray() throws IOException {
  	try {
  		rootObject = PrimitiveArrayClass.createFromCmdLine();
    	sendObjects();
    	rootObject = null;
  	} catch (InputMismatchException e) {
  		System.out.println("Invalid input");
  		return;
  	}
  }
  
  public void createRArray() throws IOException {
  	try {
  		rootObject = ReferenceArrayClass.createFromCmdLine();
    	sendObjects();
    	rootObject = null;
  	} catch (InputMismatchException e) {
  		System.out.println("Invalid input");
  		return;
  	}
  }
  
  public void createCollection() throws IOException {
  	try {
  		ArrayList<PrimitiveArrayClass> al = new ArrayList<PrimitiveArrayClass>();
  		al.add(PrimitiveArrayClass.createFromCmdLine());
  		al.add(PrimitiveArrayClass.createFromCmdLine());
  		
  		rootObject = al;
  		sendObjects();
    	rootObject = null;
  	} catch (InputMismatchException e) {
  		System.out.println("Invalid input");
  		return;
  	}
  }
  
  public void sendObjects() throws IOException {
		Client.send(Serializer.serialize(rootObject), host, port);
  }
  
  public void listObjects() {
  	for ( String k : objects.keySet() ) {
  		System.out.println(String.format("%s (%s)", k, objects.get(k).getClass().getSimpleName()));
  	}
  }
  
  public boolean runMenu() throws Throwable {
  	return MenuCommand.runMenu(rootCmd);
  }

  private String getNameFromCmdLine(){
  	System.out.print("Enter class name: ");
  	return new Scanner(System.in).nextLine();
  }
  
  public static void main(String[] args) throws Throwable {
  	if(args.length < 2) {
  		System.out.println("host or port not specified");
  		return;
  	}
  	
	  ObjectCreator oc = new ObjectCreator(args[0], Integer.parseInt(args[1]));
	  while(oc.runMenu());
  }
}
