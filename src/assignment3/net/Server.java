package assignment3.net;

import org.jdom.*;
import org.jdom.input.*;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;

import java.io.*;
import java.net.*;
import assignment3.*;

public class Server {
	private ServerSocket sock;
	public DocumentAcceptor acceptor;
	
	public Server(int port) throws IOException {
		sock = new ServerSocket(port);
	}
	
	public void listen() throws Throwable {
		System.out.println("Started listening on " + sock.getLocalPort());
		for(;;) {
			Socket clientSock = sock.accept();
			System.out.println("Accepted connection from " + clientSock.getPort());
			handle(clientSock.getInputStream());
			clientSock.close();
		}
	}
	
	private void handle(InputStream stream) throws Throwable {
		SAXBuilder builder = new SAXBuilder();
		
		Document doc = null;
		try {
			doc = builder.build(stream);
		} catch (IOException e) {
			
		} catch (JDOMException e) {
			
		}
		
		if(acceptor != null) {
			acceptor.accept(doc);
		}
	}
}
