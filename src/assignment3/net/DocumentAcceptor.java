package assignment3.net;

public interface DocumentAcceptor {
	public void accept(org.jdom.Document doc) throws Throwable;
}
