package assignment3.net;

import java.io.IOException;
import java.net.*;
import org.jdom.*;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;

import java.io.*;

public class Client {
	public static void send(Document doc, String host, int port) throws IOException {
		Socket sock = new Socket(host, port);
		
		XMLOutputter outputter = new XMLOutputter(Format.getRawFormat());
		
		OutputStreamWriter str = new OutputStreamWriter(sock.getOutputStream());
		str.write(outputter.outputString(doc));
		str.flush();
		
		sock.close();
	}
}
